# README #

This README would normally document whatever steps are necessary to get your application up and running.

### build ###
npm install
add the localization files by following https://github.com/stefalda/ReactNativeLocalization
react-native run-ios

### TODO ###

* sound/vibrate
* banner network
* subscription
* affiliate links for product
* monthly notification, remain to use this app
* multi-lang
* rate this app
* share on fb
* user_id for banner clicked user
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Important notes ###

* First launch impression.  This app is expected to be used by a small amount of users.  Once they downloaded it, we don't expect they will upgrade and also we don't expect the growth rate will be big.  So the features in first launch must have all key features.
* In app description, could mention this is a training program instead of manage your work.(e.g. 30mins, 40mins)
