let RNFS = require('react-native-fs');

export default class Asset {
    constructor() {
        this.state = {};
        this.state.homeBundle = {
            wallpaper : require('../assets/images/jn_insta_18131155348_20f99a93ff_k.jpg')
        };
        this.state.breakBundle = [{
            wallpaper : require('../assets/images/jn_insta_17981246061_9224f962f1_h.jpg')
        }];
        this.state.finishBundle = {
            wallpaper : require('../assets/images/jn_insta_18020301400_2e78fc5134_k.jpg'),
            more_label : 'Find out more',
            more_link : 'https://workcycle.me'
        };
        this.assetPath = RNFS.DocumentDirectoryPath + '/' + 'assets';
        RNFS.mkdir(this.assetPath);

        if (__DEV__) {
            this.endpoint = 'http://192.168.56.101:8000/api/v2/assetbundles/?version=1.0&fields=_,id,bundletype,message,readmore_url,readmore_label,photo_url';
        } else {
            this.endpoint = 'https://workcycle.me/api/v2/assetbundles/?version=1.0&fields=_,id,bundletype,message,readmore_url,readmore_label,photo_url';
        }
    }

    loadEndpoint() {
        let instance = this;

        fetch(instance.endpoint, {
            headers: {
                'Cache-Control': 'no-cache'
            }
        }).then((response) => response.json())
            .then((responseData) => {
                let folder = Date.now();
                let tmpPath = RNFS.DocumentDirectoryPath + '/tmp' + folder;//store the downloading files
                let finalPath = this.assetPath;//final destination
                RNFS.mkdir(tmpPath)
                    .then((success) => {
                        let phome, pbreak, pfinish;
                        for (let i = 0; i < responseData.items.length; i++) {
                            if (responseData.items[i].bundletype == 'home') {
                                phome = new Promise(function (resolve, reject) {
                                    let files = [];
                                    let values = {};
                                    values.message = responseData.items[i].message;
                                    values.more_link = responseData.items[i].readmore_url;
                                    values.more_label = responseData.items[i].readmore_label;
                                    instance.downloadFile(responseData.items[i].photo_url, tmpPath)
                                        .then((f) => {
                                            values.wallpaper = f;
                                            files.push(values);
                                            resolve(files);
                                        })
                                        .catch((e) => {
                                            reject(e);
                                        });
                                });
                            }

                            if (responseData.items[i].bundletype == 'break') {
                                pbreak = new Promise(function (resolve, reject) {
                                    let files = [];
                                    let values = {};
                                    values.message = responseData.items[i].message;
                                    values.more_link = responseData.items[i].readmore_url;
                                    values.more_label = responseData.items[i].readmore_label;
                                    instance.downloadFile(responseData.items[i].photo_url, tmpPath)
                                        .then((f) => {
                                            values.wallpaper = f;
                                            files.push(values);
                                            resolve(files);
                                        })
                                        .catch((e) => {
                                            reject(e);
                                        });
                                });
                            }

                            if (responseData.items[i].bundletype == 'finish') {
                                pfinish = new Promise(function (resolve, reject) {
                                    let files = [];
                                    let values = {};
                                    values.message = responseData.items[i].message;
                                    values.more_link = responseData.items[i].readmore_url;
                                    values.more_label = responseData.items[i].readmore_label;
                                    instance.downloadFile(responseData.items[i].photo_url, tmpPath)
                                        .then((f) => {
                                            values.wallpaper = f;
                                            files.push(values);
                                            resolve(files);
                                        })
                                        .catch((e) => {
                                            reject(e);
                                        });
                                });
                            }
                        }

                        //all files downloaded, move from tmp to final path
                        Promise.all([phome, pbreak, pfinish]).then(function (values) {
                            //remove the old assets
                            RNFS.unlink(finalPath)
                                .then((success) => {
                                    //rename tmp to assets
                                    RNFS.moveFile(tmpPath, finalPath)
                                        .then((success) => {
                                            //assign the new filename
                                            //we cannot use require() as the image is not in static assets
                                            instance.state.homeBundle.wallpaper = {uri: finalPath + '/' + values[0][0].wallpaper};
                                            instance.state.breakBundle[0].wallpaper = {uri: finalPath + '/' + values[1][0].wallpaper};
                                            instance.state.finishBundle.wallpaper = {uri: finalPath + '/' + values[2][0].wallpaper};
                                            instance.state.finishBundle.more_label = values[2][0].more_label;
                                            instance.state.finishBundle.more_link = values[2][0].more_link;
                                            instance.state.finishBundle.message = values[2][0].message;

                                            //finally, in case there was left over from previous download(e.g. app exit in the middle)
                                            instance.cleanTmpFolder();
                                        });
                                })
                                .catch((e) => {
                                    instance.cleanTmpFolder();
                                });
                        }, function (e) {
                            instance.cleanTmpFolder();
                        });

                    })
                    .catch((e) => {
                        instance.cleanTmpFolder();
                    });
            })
            .catch((error) => {
                instance.cleanTmpFolder();
            });
    }

    downloadFile(url, path) {
        let unique = Date.now().toString(36) + Math.random().toString(36).substr(2, 5);
        let filename = path + '/' + unique;
        return new Promise(function(resolve, reject){
            let header = null;
            RNFS.downloadFile(url, filename, function(arg){header = arg.headers["Content-Type"]})
                .then((response) => {
                    if(response.statusCode == 200){
                        let ext = null;
                        switch (header){
                            case 'image/jpeg':
                                ext = 'jpg';
                                break;
                            case 'image/png':
                                ext = 'png';
                                break;
                            case 'image/gif':
                                ext = 'gif';
                                break;
                            default:
                                break;
                        }
                        if(ext){
                            unique = unique + '.' + ext;
                            RNFS.moveFile(filename, filename + '.' + ext)
                                .then((success) => {
                                    resolve(unique);
                                })
                                .catch((e) => {
                                    reject(e);
                                });
                        }else{
                            resolve(unique);
                        }
                    }else{
                        reject(response);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
        });
    }

    cleanTmpFolder(){
        RNFS.readDir(RNFS.DocumentDirectoryPath)
        .then((result) => {
            result.map(function(obj){
                if(obj.isDirectory()){
                    if(obj.path.match('\/tmp[0-9]+$')){
                        RNFS.unlink(obj.path)
                        .then((success) => {
                        })
                        .catch((e) => {
                        });
                    }
                }
            });
        })
        .catch((err) => {
        });
    }
}
