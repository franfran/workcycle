/*
 * Since vendor/countdown is a module function
 * In this constructor, we return the vendor/countdown object with our extended functions
 */
import countdown from '../vendor/countdown';

class Countdown extends countdown {
    constructor() {
        super();

        let c = countdown;
        //remove all formating for the countdown
        c.setLabels(
            ' | | | | | | | | | | ',
            ' | | | | | | | | | | ',
            '',
            '',
            '', function (n) {
                return n.toString();
            });
        c.secondsToTimeString = Countdown.secondsToTimeString;

        return c;
    }
    static secondsToTimeString (seconds) {
        //http://stackoverflow.com/questions/6312993/javascript-seconds-to-time-string-with-format-hhmmss
        return (new Date(seconds * 1000)).toUTCString().match(/(\d\d:\d\d:\d\d)/)[0];
    }
}

export default Countdown;