import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    WebView,
    Dimensions,
    TouchableOpacity,
    StatusBar,
    Linking
} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import { Actions } from "react-native-router-flux";
import { connect } from 'react-redux';
import Button from 'apsl-react-native-button';

let strings = new LocalizedStrings({
    en: {
        title: "",
    },
});

class BannerPage extends React.Component {
    constructor(props) {
        super(props);
    }
    backButtonPressed = (event) => {
        Actions.home();
    }
    rightButtonPressed = (event) => {
        this.refs.webview.reload();    
    }
    onNavigationStateChange (navState) {
    }
    onShouldStartLoadWithRequest (event) {
        Linking.openURL(event.url);
        return false;
    }
    render () {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" hidden={false} />
                <View style={ styles.navbar }>
                    <TouchableOpacity
                        style={ styles.backButton }
                        onPress={ this.backButtonPressed }>
                        <Text
                            style={ styles.backButtonText }>
                            &lt;
                        </Text>
                    </TouchableOpacity>
                    <View style={styles.navBarTitle}>
                        <Text style={ styles.navBarTitleText }>
                            {strings.title}
                        </Text>
                    </View>
                    <TouchableOpacity
                        style={ styles.rightButton }
                        onPress={ this.rightButtonPressed }>
                        <Text
                            style={ styles.rightButtonText }>
                            &#8635;
                        </Text>
                    </TouchableOpacity>
                </View>
                <WebView
                    automaticallyAdjustContentInsets={false}
                    style={styles.webView}
                    source={{uri: this.props.url}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    decelerationRate="normal"
                    onNavigationStateChange={this.onNavigationStateChange}
                    onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
                    startInLoadingState={true}
                    scalesPageToFit={true}
                    ref="webview"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        borderWidth: 0,
        flexDirection: 'column',
        backgroundColor: '#ffffff',
    },
    //left flex: 1, title flex: 3, right flex: 1
    navbar: {
        paddingTop: 20,
        backgroundColor: '#004c40',
        borderWidth: 0,
        width: Dimensions.get('window').width,
        height: 64, //status bar 20 + 44
        flexDirection: 'row',
        alignItems: 'center',
    },
    backButton: {
        borderWidth: 0,
        paddingLeft: 16,
        alignItems: 'center',
        flexDirection: 'row',
    },
    backButtonText: {
        borderWidth: 0,
        fontSize: 30,
        color: '#ffffff',
        fontFamily: 'NotoSerif-Bold',
    },
    navBarTitle: {
        borderWidth: 0,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        flex: 3,
    },
    navBarTitleText: {
        fontSize: 20,
        fontFamily: 'NotoSerif',
        borderWidth: 0,
        color: '#ffffff',
    },
    rightButton: {
        borderWidth: 0,
        borderColor: '#fff',
        paddingRight: 16,
        alignItems: 'center',
        flexDirection: 'row',
    },
    rightButtonText: {
        borderWidth: 0,
        fontSize: 30,
        color: '#ffffff',
        fontFamily: 'NotoSerif-Bold',
    },
    webView: {
        alignSelf: 'stretch',
        flex: 1,
        width: Dimensions.get('window').width,
    }
});

function mapStateToProps(state) {
  return {
      state: {
      }
  }
}

export default connect(mapStateToProps)(BannerPage);
