import React, { PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import { Actions } from "react-native-router-flux";
import { connect } from 'react-redux';
import Countdown from '../lib/Countdown';

let countdown = new Countdown();
let strings = new LocalizedStrings({
    en: {
        break: "BREAK",
    }
});

class BreakPage extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount (){
        this.timer = null;

        this.breakIntervalSeconds = this.props.state.breakIntervalSeconds;
        this.breakBundle = this.props.state.breakBundle;
        this.countdown = this.breakIntervalSeconds;
        this.breakPageCount = this.props.state.breakPageCount;

        //default is taking the first wallpaper, see if we have more from server
        this.wallpaper = this.breakBundle[0].wallpaper;
        if(this.breakPageCount < this.breakBundle.length){
            this.wallpaper = this.breakBundle[this.breakPageCount].wallpaper;
        }
    }
    componentDidMount () {
        //start the countdown
        let milliseconds = this.breakIntervalSeconds * 1000;
        let now = new Date();
        let targetDate = new Date(now.getTime() + milliseconds);
        let that = this;
        this.timer = countdown(targetDate,
            function (ts) {
                if (ts == 0) {
                    that.cancelTimer();
                    return;
                }
                let t = ts.toString();
                //time adjustment for the screen transition delayed for 1 second
                //i.e. countdown function is correct, but render() has delayed in next cycle
                //so we do t - 1 for displaying it correctly
                that.countdown = t - 1;
            },
            countdown.SECONDS
        );
    }
    cancelTimer() {
        window.clearInterval(this.timer);
        this.timer = null;
        Actions.pop();
    }
    updateCountDown(){
        return countdown.secondsToTimeString(this.countdown);
    }
    render () {
        return (
            <View style={styles.container}>
                <Image
                    source={ this.wallpaper }
                    style={ styles.backgroundImage }/>
                <Text style={styles.break}>{strings.break}</Text>
                <Text style={styles.countdown}>{this.updateCountDown()}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        borderWidth: 0,
        flexDirection: 'column',
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        resizeMode: 'cover',
        flex: 1,
        width: null,
        height: null,
    },
    break: {
        fontSize: 20,
        textAlign: 'center',
        backgroundColor: 'transparent',
        color: '#fff',
        textShadowRadius: 3,
        textShadowColor: '#000000',
        textShadowOffset: {width:1, height:1},
    },
    countdown: {
        fontSize: 20,
        textAlign: 'center',
        backgroundColor: 'transparent',
        color: '#fff',
        textShadowRadius: 3,
        textShadowColor: '#000000',
        textShadowOffset: {width:1, height:1},
    },

});

function mapStateToProps(state) {
  return {
      state: {
          breakIntervalSeconds: state.workcycle.breakIntervalSeconds,
          breakBundle: state.workcycle.asset.state.breakBundle,
          breakPageCount: state.workcycle.breakPageCount,
      }
  }
}

export default connect(mapStateToProps)(BreakPage);
