import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    WebView,
    Dimensions,
    TouchableOpacity,
    PixelRatio,
    Alert,
    ActivityIndicator
} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import { Actions } from "react-native-router-flux";
import { connect } from 'react-redux';
import Button from 'apsl-react-native-button';
import t from 'tcomb-form-native';

let strings = new LocalizedStrings({
    en: {
        title: "Settings",
        email: "Email",
        newsletter_subscription: "Newsletter subscription",
        submit: "Submit",
        your_email: "Your email",
        connection_error: "Connection error, please try again later",
        thankyou: "Thank you",
        intro: "Thank you for downloading WorkCycle, we believe it can help you to build your new working habit.",
        intro2: "Subscribe below and rediscover the beauty of life!",
        privacy_policy: "Privacy Policy"
    }
});

const Form = t.form.Form;
const EMAIL_REGEX = new RegExp("[^\\.\\s@][^\\s@]*(?!\\.)@[^\\.\\s@]+(?:\\.[^\\.\\s@]+)*");
const EmailField = t.refinement(t.String, function (email) {
    return EMAIL_REGEX.test(email);
});
//define form domain model
const SubscriptionModel = t.struct({
    email: EmailField
});

const formStylesheetDefault = JSON.parse(JSON.stringify(t.form.Form.stylesheet));//clone the freeze object
const formStylesheet = {
    ...formStylesheetDefault,
    controlLabel: {
        ...formStylesheetDefault.controlLabel,
        normal: {
            ...formStylesheetDefault.controlLabel.normal,
            height: 0
        },
        error: {
            ...formStylesheetDefault.controlLabel.error,
            height: 0
        }
    },
    textbox: {
        ...formStylesheetDefault.textbox,
        normal: {
            ...formStylesheetDefault.textbox.normal,
            fontFamily: 'NotoSerif',
            color: '#EEEEEE'
        },
        error: {
            ...formStylesheetDefault.textbox.error,
            fontFamily: 'NotoSerif',
            color: '#EEEEEE'
        }
    }
};

const options = {
    fields: {
        email: {
            placeholder: strings.your_email,
            label: strings.email,
            placeholderTextColor: '#C7C7C7',
            autoCorrect: false,
            autoCapitalize: 'none',
            keyboardType: 'email-address',
        }
    },
    stylesheet: formStylesheet
};

class SettingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.refreshState();
    }

    refreshState() {
        return {
            ...this.props.state,
        };
    }

    backButtonPressed = (event) => {
        Actions.pop();
    };

    submitButtonPressed = (event) => {
        let value = this.refs.form.getValue();
        let subscription_endpoint;
        if (__DEV__) {
            subscription_endpoint = 'http://192.168.56.101:8000/api/v2/subscription';
        } else {
            subscription_endpoint = 'https://workcycle.me/api/v2/subscription';
        }

        if (value) {
            this.setState({submitting: true});
            fetch(subscription_endpoint, {
                method: 'POST',
                headers: {
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'email': value.email
                })
            }).then((response) => response.json())
                .then((responseData) => {
                    if (responseData.status == true) {
                        Alert.alert('', strings.thankyou);
                    } else {
                        alert(strings.connection_error);
                    }
                    this.setState({submitting: false});
                })
                .catch((error) => {
                    alert(strings.connection_error);
                    this.setState({submitting: false});
                });
        }
    };
    privacyLinkPressed = () => {
        let privacy_url;
        if (__DEV__) {
            privacy_url = 'http://192.168.56.101:8000/privacy-policy';
        } else {
            privacy_url = 'https://workcycle.me/privacy-policy';
        }
        Actions.banner({url:privacy_url});
    };
    subscriptionForm = () => {
        return (
            <Form
                ref="form"
                type={SubscriptionModel}
                options={options}
            />
        );
    };
    submitButton = () => {
        button = <Button
            style={styles.button}
            textStyle={styles.buttonText}
            isLoading={false}
            onPress={this.submitButtonPressed}>
            {strings.submit}
        </Button>

        if(this.state.submitting){
            button = <ActivityIndicator />
        }

        return button;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={ styles.navbar }>
                    <TouchableOpacity
                        style={ styles.backButton }
                        onPress={ this.backButtonPressed }>
                        <Text
                            style={ styles.backButtonText }>
                            &lt;
                        </Text>
                    </TouchableOpacity>
                    <View style={styles.navBarTitle}>
                        <Text style={ styles.navBarTitleText }>
                            {strings.title}
                        </Text>
                    </View>
                    <View style={styles.rightButton}/>
                </View>
                <View style={styles.introWrapper}>
                    <Text style={styles.introText}>{strings.intro}</Text>
                </View>
                <View style={styles.introWrapper}>
                    <Text style={styles.introText}>{strings.intro2}</Text>
                </View>
                <View style={styles.subscriptionWrapper}>
                    <Text style={styles.subscriptionText}>{strings.newsletter_subscription}</Text>
                    {this.subscriptionForm()}
                    <Text style={styles.termsText} onPress={this.privacyLinkPressed}>{strings.privacy_policy}</Text>
                    {this.submitButton()}
                </View>
            </View>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderWidth: 0,
        flexDirection: 'column',
    },
    //left flex: 1, title flex: 3, right flex: 1
    navbar: {
        paddingTop: 20,
        backgroundColor: '#004c40',
        borderWidth: 0,
        width: Dimensions.get('window').width,
        height: 64, //status bar 20 + 44
        flexDirection: 'row',
        alignItems: 'center',
    },
    backButton: {
        borderWidth: 0,
        flex: 1,
        paddingLeft: 16,
        alignItems: 'center',
        flexDirection: 'row',
    },
    backButtonText: {
        borderWidth: 0,
        fontSize: 30,
        color: '#ffffff',
        fontFamily: 'NotoSerif-Bold',
    },
    navBarTitle: {
        borderWidth: 0,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        flex: 3,
    },
    navBarTitleText: {
        fontSize: 20,
        fontFamily: 'NotoSerif',
        borderWidth: 0,
        color: '#ffffff',
    },
    rightButton: {
        flex: 1,
        borderWidth: 0,
    },
    introWrapper: {
        alignSelf: 'stretch',
        backgroundColor: '#00796b',
        padding: 16,
    },
    introText: {
        fontSize: 16,
        fontFamily: 'NotoSerif',
        color: '#eeeeee'
    },
    subscriptionWrapper: {
        alignSelf: 'stretch',
        backgroundColor: '#00796b',
        flex: 1,
        padding: 16,
        borderWidth: 0,
    },
    subscriptionText: {
        fontSize: 16,
        fontFamily: 'NotoSerif',
        color: '#eeeeee',
    },
    termsText: {
        fontSize: 14,
        fontFamily: 'NotoSerif',
        color: '#C7C7C7',
    },
    button: {
        backgroundColor: '#004c40',
        borderWidth: 0,
        width: 250,
        height: 50,
        borderRadius: 30 / PixelRatio.get(),
        overflow: 'hidden',
        marginTop: 20,
        alignSelf: 'center',
    },
    buttonText: {
        fontSize: 20,
        fontFamily: 'NotoSerif',
        color: '#FFFFFF'
    },

    subscriptionEmail: {
        borderWidth: 1,
        marginTop: 10,
        height: 30,
    }
});

function mapStateToProps(state) {
    return {
        state: {
            subscriptionValue: 'n@a.com',
            submitting: false
        }
    }
}

export default connect(mapStateToProps)(SettingPage);
