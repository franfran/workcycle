import React, { PropTypes } from 'react';
import {
    StyleSheet,
    PixelRatio,
    View,
    Text,
    Image,
    TouchableOpacity,
    Easing,
    StatusBar
} from 'react-native';

import Button from 'apsl-react-native-button';
import SnapSlider from 'react-native-snap-slider';
import LocalizedStrings from 'react-native-localization';
import FadeToogle from 'react-native-fade-toogle';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Countdown from '../lib/Countdown';

let countdown = new Countdown();
let strings = new LocalizedStrings({
    en: {
        start: "Start",
        stop: "Stop",
        mins: "mins",
        hour: "hour",
        hours: "hours",
        loading: "Loading"
    }
});

class HomePage extends React.Component {
    static propTypes = {
        routes: PropTypes.object,
      };
    constructor(props) {
        super(props);
        //workTimeOptions value unit in seconds
        this.workTimeOptions = [
                {
                    value: 900,
                    label: '15 ' + strings.mins
                },
                {
                    value: 1800,
                    label: '30 ' + strings.mins
                },
                {
                    value: 3600,
                    label: '1 ' + strings.hour
                },
                {
                    value: 7200,
                    label: '2 ' + strings.hours
                },
            ];
        if (__DEV__) {
            this.workTimeOptions[0].value = 9;
        }
        this.timer = null;
        this.state = this.refreshState();
        this.breakSeconds = 0;//how long will take a break, now it is dynamically determined by getbreakSeconds() in runtime
        //this.startButtonPressed = this.startButtonPressed.bind(this);
        //this._settingButtonPressed = this._settingButtonPressed.bind(this);
    }
    refreshState() {
        let buttonLabel;
        if(this.props.state.home_btn_start){
            buttonLabel = strings.start;
        }else{
            buttonLabel = strings.stop;
        }
        return {
            ...this.props.state,
            buttonLabel: buttonLabel
        };
    }
    componentWillMount ()  {
        this.props.loadDatabase();
        this.wallpaper = this.state.asset.state.homeBundle.wallpaper;
    }
    slidingComplete() {
        console.log("slidingComplete");
    }
    cancelTimer() {
        window.clearInterval(this.timer);
        this.timer = null;
        this.toggleStart();
    }
    getbreakSeconds(workSeconds){
        let r;

        switch (workSeconds){
            case 7200:
                r = 900;//15mins
                break;
            case 3600:
            case 1800:
                r = 600;//10mins
                break;
            default:
                r = Number.MAX_VALUE;//never
                r = 5;
                break;
        }

        return r;
    }
    //using arrow function for .bind(this)
    //http://egorsmirnov.me/2015/08/16/react-and-es6-part3.html
    startButtonPressed = (event) => {
        let seconds = this.workTimeOptions[this.refs.slider.state.item].value;//should set the settings.workSeconds
        let milliseconds = seconds * 1000;
        let now = new Date();
        let targetDate = new Date(now.getTime() + milliseconds);
        let that = this;
        if (this.timer) {
            //stop the countdown
            this.cancelTimer();
            return;
        }

        //reset number of breakPage shown
        this.props.breakPageCount(-1);

        //start the countdown
        this.toggleStart(this.refs.slider.state.item);

        let breakSeconds = this.getbreakSeconds(seconds);
        this.breakSeconds = 0;
        this.timer = countdown(targetDate, function (ts) {
            that.props.countdown(ts.toString(), seconds);
            if(that.breakSeconds == 0){
                that.breakSeconds = breakSeconds;
            }else{
                that.breakSeconds = that.breakSeconds - 1;
            }
            if (ts == 0) {
                that.cancelTimer();
                Actions.finish();
                //we only update the wallpaper in the next scene cycle
                that.wallpaper = that.state.asset.state.homeBundle.wallpaper;
            }
            if ((that.breakSeconds == 0) && (ts > that.state.breakIntervalSeconds)){
                that.startBreakInterval();
            }
          },
          countdown.SECONDS
        );
        //update new assets from server
        this.state.asset.loadEndpoint();
    };
    //manage the start button state and slider toggle
    toggleStart(option = null){
        //hide the slider, since disable='true' not working for this component
        let r = this.refs.fade;
        r.fadeToggle();

        //avoid using this.props.dispatch()
        //while syntax this.props.dispatch(this.props.toggleStart) is fine
        //however, this.props.dispatch(this.props.toggleStart()) will cause double calling of toggleStart()!!
        this.props.toggleStart(option);
    }
    updateCountDown() {
        return countdown.secondsToTimeString(this.state.countdown);
    }
    startBreakInterval() {
        this.props.breakPageCount(1);
        Actions.break();
    }
    render() {
        this.state = this.refreshState();

        if (this.state.isLoading === false) {
            return (
                <View style={ styles.container }>
                    <StatusBar barStyle="light-content" hidden={false} />
                    <Image
                        source={ this.wallpaper }
                        style={ styles.backgroundImage }/>
                    <View style={ styles.navbar }>
                        <TouchableOpacity
                            style={ styles.navbarIconTouchable }
                            onPress={ Actions.setting }>
                            <Text
                                style={ styles.navbarIcon }>
                                &#9781;
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <FadeToogle
                        ref="fade"
                        component="View"
                        easing={ Easing.elastic(2) }
                        style={ styles.SnapSliderWrapper }>
                        <SnapSlider
                            ref="slider"
                            key="slider"
                            containerStyle={ styles.snapsliderContainer }
                            style={ styles.snapslider }
                            itemWrapperStyle={ styles.snapsliderItemWrapper }
                            itemStyle={ styles.snapsliderItem }
                            items={ this.workTimeOptions }
                            defaultItem={ this.state.defaultItem }
                            onSlidingComplete={ this.slidingComplete }
                            minimumTrackTintColor="#8fd9d0"
                        />
                    </FadeToogle>
                    <Text style={ styles.countdown }>
                        { this.updateCountDown() }
                    </Text>
                    <Button
                        style={ styles.button }
                        textStyle={ styles.buttonText }
                        onPress={ this.startButtonPressed }>
                        { this.state.buttonLabel }
                    </Button>
                </View>
            );
        }else{
            return (
                <View style={ styles.container }>
                    <Text>
                        { strings.loading } ...
                    </Text>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        borderWidth: 0,
        flexDirection: 'column'
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        resizeMode: 'cover',
        flex: 1,
        width: null,
        height: null
    },
    navbar: {
        backgroundColor: 'rgba(0,0,0,0)',
        borderWidth: 0,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        justifyContent: 'space-between',
        height: 56,
        flexDirection: 'row'
    },
    navBarButtonText: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    navbarIconTouchable: {
        position: 'absolute',
        backgroundColor: '#00796b',
        borderRadius: 5,
        top: 36, //STATUSBAR_HEIGHT + 16, STATUSBAR_HEIGHT_IOS:20 STATUSBAR_HEIGHT_AN:24
        right: 16,
        height: 30,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    navbarIcon: {
        borderWidth: 0,
        fontSize: 20,
        borderRadius: 15,
        backgroundColor: 'transparent',
        color: '#ffffff'
    },
    countdown: {
        fontSize: 20,
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontFamily: 'NotoSerif-Italic',
        color: '#ffffff',
        marginTop: 20,
        textShadowRadius: 3,
        textShadowColor: '#000000',
        textShadowOffset: {width:1, height:1},
        paddingLeft: 5,
        paddingRight: 5
    },
    button: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: '#ffffff',
        width: 250,
        height: 50,
        borderRadius: 30 / PixelRatio.get(),
        overflow: 'hidden',
        marginTop: 20,
        alignSelf: 'center',
    },
    buttonText:{
        fontSize: 20,
        fontFamily: 'NotoSerif',
        textShadowRadius: 3,
        textShadowColor: '#000000',
        textShadowOffset: {width:1, height:1},
        color: '#FFFFFF'
    },
    SnapSliderWrapper: {
        backgroundColor: 'transparent'
    },
    snapsliderContainer: {
        borderWidth: 0,
        backgroundColor: 'transparent',
        width: 300,
    },
    snapslider: {
        borderWidth: 0,
    },
    snapsliderItemWrapper: {
        borderWidth: 0
    },
    snapsliderItem: {
        borderWidth: 0,
        fontSize: 16,
        fontFamily: 'NotoSerif',
        color: '#fff',
        textShadowRadius: 3,
        textShadowColor: '#000000',
        textShadowOffset: {width:1, height:1},
    }
});

//for better performance, map sliced state for needed state only
//https://github.com/reactjs/react-redux/blob/master/docs/api.md
function mapStateToProps(state, ownProps) {
  return {
      state: {
          countdown: state.workcycle.countdown,
          home_btn_start: state.workcycle.home_btn_start,
          breakIntervalSeconds: state.workcycle.breakIntervalSeconds,
          isLoading: state.workcycle.isLoading,
          defaultItem: state.workcycle.defaultItem,
          asset: state.workcycle.asset
      }
  };
}

export default connect(mapStateToProps)(HomePage);
