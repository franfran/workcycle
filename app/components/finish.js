import React, { PropTypes } from 'react';
import {
    StyleSheet,
    PixelRatio,
    Text,
    View,
    Image,
    StatusBar
} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import { Actions } from "react-native-router-flux";
import { connect } from 'react-redux';
import Button from 'apsl-react-native-button';

let strings = new LocalizedStrings({
    en: {
        next: "Next",
        finish_message: "Well done!  You have finished."
    }
});

class FinishPage extends React.Component {
    constructor(props) {
        super(props);
        this.finishBundle = this.props.state.finishBundle;
    }
    renderBannerDetails = () => {
        let details;
        if(this.finishBundle.more_label){
            if(this.finishBundle.more_label.length > 0) {
                details = (
                <View style={styles.bannerButtonRow}>
                    <Button style={styles.bannerButton}
                    onPress={this.moreButtonPressed}
                    textStyle={styles.bannerButtonText}>
                        {this.finishBundle.more_label}
                    </Button>
                </View>);
            }
        }

        return details;
    };
    moreButtonPressed = () => {
        Actions.banner({url:this.finishBundle.more_link});
    };
    dismissButtonPressed = () => {
        Actions.pop({refresh: {}});//refresh, let the parent to reload the state
    }
    render () {
        return (
            <View style={styles.container}>
                <StatusBar hidden={true} />
                <View style={styles.banner}>
                    <Image
                        source={ this.finishBundle.wallpaper }
                        style={ styles.backgroundImage }
                        onPress={this.moreButtonPressed} />
                    { this.renderBannerDetails() }
                </View>
                <View style={styles.footer}>
                    <Text style={styles.footerText}>{ strings.finish_message }</Text>
                    <Button
                        style={ styles.footerButton }
                        onPress={ this.dismissButtonPressed }
                        textStyle={ styles.footerButtonText }>
                        { strings.next }
                    </Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        borderWidth: 0,
        flexDirection: 'column',
        backgroundColor: '#e7e7e7',
    },
    banner: {
        alignSelf: 'stretch',
        flex: 4,
    },
    footer: {
        alignSelf: 'stretch',
        flex: 1,
        backgroundColor: '#00796b',
        justifyContent: 'center',
    },
    /* 750x925 ratio */
    backgroundImage: {
        resizeMode: 'contain',
        width: null,
        height: null,
        flex: 1,
    },
    bannerButtonRow: {
        paddingTop: 15,
        paddingBottom: 15,
    },
    bannerButton: {
        backgroundColor: 'transparent',
        width: 150,
        height: 50,
        borderRadius: 30 / PixelRatio.get(),
        borderWidth: 1,
        borderColor: '#7c7c7c',
        overflow: 'hidden',
        alignSelf: 'center',
    },
    bannerButtonText: {
        color: '#000000',
    },
    footerButton: {
        marginRight: 0,
        borderWidth: 0,
        width: 150,
        height: 50,
        overflow: 'hidden',
        alignSelf: 'flex-end',
        marginRight: 15,
    },
    footerButtonText: {
        color: '#eeeeee',
        textAlign: 'right',
    },
    footerText: {
        paddingTop: 15,
        paddingLeft: 15,
        fontSize: 16,
        textAlign: 'left',
        backgroundColor: 'transparent',
        fontFamily: 'NotoSerif',
        color: '#eeeeee'
    },
});

function mapStateToProps(state) {
  return {
      state: {
          finishBundle: state.workcycle.asset.state.finishBundle
      }
  }
}

export default connect(mapStateToProps)(FinishPage);
