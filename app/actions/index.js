import store from 'react-native-simple-store';

export function toggleStart(option) {
  //memorize the slider position
  if(option !== undefined && option !== null) {
    store.save('setting', {workTimeOption: option});
  }
  return {
    type: 'TOGGLE_START',
    option: option
  };
}

//thunk middleware
export function loadDatabase() {
  return function (dispatch, getState) {
    return store.get('setting').then((s) => {
      let option = 0;
      if (s) {
        if (s.workTimeOption !== undefined && s.workTimeOption !== null) {
          option = s.workTimeOption;
        }
      }

      store.save('setting', {
        workTimeOption: option
      }).then(() => {
        dispatch(loadedDatabase(option));
      });
    });
  };
}

export function loadedDatabase(option) {
  return {
    type: 'LOADED_DATABASE',
    option: option
  };
}

export function countdown(ts, workSeconds) {
  return {
    type: 'COUNTDOWN',
    ts: ts
  };
}

export function breakPageCount(increment) {
  return {
    type: 'BREAKPAGE_COUNT',
    increment: increment
  };
}
