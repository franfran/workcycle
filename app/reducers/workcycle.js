import Asset from '../lib/Asset';

let asset = new Asset();

const initialState = {
  home_btn_start: true,
  countdown: 0,
  defaultItem: 0,
  breakIntervalSeconds: 3, //break for how long
  isLoading: true,
  vibrate: true,
  sound: 'xxx.aac',
  asset: asset,
  breakPageCount: -1,
};


export default function workcycle(state = initialState, action = {}) {
  switch (action.type) {
    case 'LOADED_DATABASE':
      return {
        ...state,
        defaultItem: action.option,
        isLoading: false
      };
    case 'TOGGLE_START':
      return {
        ...state,
        home_btn_start: !state.home_btn_start
      };
    case 'COUNTDOWN':
      return {
        ...state,
        countdown: action.ts,
        workSeconds: action.workSeconds
      };
    case 'BREAKPAGE_COUNT':
      let count = -1;
      if(action.increment > 0){
        count = state.breakPageCount + action.increment
      }
      return {
        ...state,
        breakPageCount: count
      };
    default:
      return state;
  }
}
