import { combineReducers } from 'redux';
import routes from './routes';
import workcycle from './workcycle';

export default combineReducers({
  routes,
  workcycle
});
