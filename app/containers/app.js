/*
 * if we import another Component e.g. WorkcycleApp and put under the Provider below
 * since the WorkcycleApp has connect() to auto load the state
 * the WorkcycleApp.render() will be reloaded everytime a store dispatch
 * and the RouterWithRedux will complain as it expected to be init once only
 * so we merged App.js and WorkcycleApp.js together into App.js
 */

import React, { Component } from 'react';

//redux
import { createStore, applyMiddleware, bindActionCreators } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from '../reducers/index';
import * as workcycleActions from '../actions/index';

//router
import {Scene, Router} from 'react-native-router-flux';
import { connect } from 'react-redux';
import HomePage from '../components/home';
import BreakPage from '../components/break';
import SettingPage from '../components/setting';
import FinishPage from '../components/finish';
import BannerPage from '../components/banner';

const RouterWithRedux = connect()(Router);

const store = createStore(
  reducers,
  applyMiddleware(thunk)
);

export default class App extends Component {
  render() {
    const state = store.state;
    const actions = bindActionCreators(workcycleActions, store.dispatch);
    return (
        <Provider store={store}>
          <RouterWithRedux {...state} {...actions}>
            <Scene key="root">
              <Scene key="home" component={HomePage} initial={true} hideNavBar={true}/>
              <Scene key="break" component={BreakPage} direction="vertical" hideNavBar={true}/>
              <Scene key="finish" component={FinishPage} direction="vertical" hideNavBar={true}/>
              <Scene key="banner" component={BannerPage} hideNavBar={true}/>
              <Scene key="setting" component={SettingPage} hideNavBar={true}/>
            </Scene>
          </RouterWithRedux>
        </Provider>
    );
  }
}
